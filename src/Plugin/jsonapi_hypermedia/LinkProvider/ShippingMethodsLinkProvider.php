<?php

declare(strict_types=1);

namespace Drupal\commerce_api\Plugin\jsonapi_hypermedia\LinkProvider;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\jsonapi\JsonApiResource\ResourceObject;
use Drupal\jsonapi_hypermedia\AccessRestrictedLink;
use Drupal\jsonapi_hypermedia\Plugin\LinkProviderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
* Class PaymentGatewayOnReturnLinkProvider.
*
* @JsonapiHypermediaLinkProvider(
*   id = "commerce_api.shipping.shipping_methods",
*   link_relation_type = "shipping-methods",
*   deriver = "\Drupal\commerce_api\Plugin\Derivative\OrderResourceTypeDeriver",
* )
*
* @internal
*/
final class ShippingMethodsLinkProvider extends LinkProviderBase implements ContainerFactoryPluginInterface {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new self($configuration, $plugin_id, $plugin_definition);
    $instance->entityRepository = $container->get('entity.repository');
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getLink($resource_object) {
    assert($resource_object instanceof ResourceObject);
    if (!str_starts_with($this->routeMatch->getRouteName(), 'commerce_api.checkout')) {
      return AccessRestrictedLink::createInaccessibleLink(new CacheableMetadata());
    }
    $entity = $this->entityRepository->loadEntityByUuid(
      'commerce_order',
      $resource_object->getId()
    );
    assert($entity instanceof OrderInterface);

    $cache_metadata = new CacheableMetadata();
    $cache_metadata->addCacheableDependency($entity);

    if (!$entity->hasField('shipments')) {
      return AccessRestrictedLink::createInaccessibleLink($cache_metadata);
    }

    return AccessRestrictedLink::createLink(
      AccessResult::allowed(),
      $cache_metadata,
      new Url('commerce_api.checkout.shipping_methods', [
        'commerce_order' => $entity->uuid(),
      ]),
      $this->getLinkRelationType()
    );
  }

}
